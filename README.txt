=== Plugin Name ===
Contributors: drewschulz, gwendydd
Donate link: http://witlee.com
Tags: Witlee, fashion, style, influencer
Requires at least: 3.0.1
Tested up to: 4.4.3
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Witlee for WP gives you the ability to monetize your pictures from within your blog.  It also gives you the power of the witlee engine right where you post

== Description ==

= About Witlee: =
Witlee is the leading e-commerce platform for influencers: Domain experts who've built an audience and inspire purchases through their engaging content. By implementing e-commerce best practices, we help this under-served market grow significantly.

Style influencers, fashion bloggers, are unique: They create content that is both inspiring and relate-able at the same time. No canned, stock photos - real women, real photos. They have a dedicated audience that comes for inspiration and transacts.

Yet, despite having amazing engagement, influencers are forced to use simple text links, lacking all the standard best practices we're used to in e-commerce experiences (similar items, hotness signals, recommendations, search or browse paths).

_Witlee bridges that gap._

We provide Style influencers a retail standard, e-commerce storefront that's fully automated revolving their content, and implementing all the current e-commerce best practices across desktop and mobile.

Witlee is considered by industry insiders as a game changer: The best solution in the market today.

= More about the plugin: =
The *Witlee for WP* plugin is focused on making it easy to incorporate the power of the Witlee backend right where your viewers are looking.

*   **No more images blocked by ad blockers**  Our plugin is 100% compatible with the site.  Because it's a native WordPress Plugin, it works seamlessly with your post.  It just works.
*   **Easy and fast integration**  No more searching a 3rd party site for just the right content.  Our advanced algorithms will only show products that are in stock.  Our advanced searching and tagging tools get you where you want to be quickly and efficiently.  Plus because it's a native Plugin, you're ensured to have the latest and greatest tools as they evolve with no extra effort of cutting and pasting.
*   **Automated recommendations**  We're not limited to just the items you created for your pick list, we will *automatically* give recommended products and maximize your profits
*   *No broken links*.  We're constantly updating the inventory to ensure that only the latest products are displayed and items that are on sale have the latest prices
*   **Advanced Technology** As a Silicon valley company focused on the Style Influencer space, our seasoned technology and business acumen gives you the best and most relevant content when you need it.


== Installation ==

1. Upload `witlee-for-wp.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to the settings page and fill in the details to get your store and code up and running
4. Once you have installed and set up the *Witlee for WP*, feel free to email us at care@witlee.com to get detailed instructions on next steps.

== Frequently Asked Questions ==

= Why do I need to allow tracking =

Witlee will provide you information about visitors and conversion.  Also as part of the platform, we provide recommentations to your users to maximize your profit.  In order for us to track the purchases that happen on your site and to give you detailed anaytics, we need to track actions on your site.

= Why should I link to Witlee =

The witlee website is not only for influencers like you who have a website, we also support influencers who don't have a website.  By allowing users to link to the witee.com website, you provide a lot more ways for your content to be discoverable.  People will discover your content on witlee.com as well your users have a richer experience on the witlee.com site.

= Why do you want my Instagram Username =

We provide you a seamless way to take your Instagram posts and tag those pictures so that it minimizes the amount of effort you have to do with your pictures.  This username also doubles as your witlee username.  It allows us to match up your content directly to your site.

= Can I only use my Instagram Images =

Instagram update allows us to automate and notify you when changes happen, but the *Witlee for WP* plugin also allows you to take the pictures you already have in your gallery and include those as part of the Witlee experience.

= Is it better to have my own store page or to link out to witlee.com =

Because of theme variances and lack of mobile support, we recommend that you **don't** use the embedded store options as many themes don't have a great support.  However, we have tested the witlee store with many themes and have protected it from site bleed-over as much as possible.  We allow you to use a page on your site to be the representation of the witlee store directly in your blog.


== Screenshots ==

TBD

== Changelog ==
= 1.1.2 =
* correct version number

= 1.1.1 =
* fixed potential js error on pages without carousel

= 1.1.0 =
* added ability to upload images to Witlee
* changed shortcode user interface to load 20 images at a time
* switched to new Google Analytics code
* load plugin assets throughout site so excerpts work
* make sure link colors won't be over-written by theme CSS

= 1.0.5 =
* Fixed margin bug on iOS Safari
* Load more image tiles in shortcode UI

= 1.0.4 =
* Adding the SOLD OUT flag when an item is marked as sold out in the system.  It's a clear indicator to the users

= 1.0.3 =
* Cleaned up support for embedded store that is available only by invitation.

= 1.0.2 =
* Added additional assets and removed the embedded store

= 1.0.1 =
* Fixed bug that caused pop-ups to break page layout in iOS Safari

= 1.0 =
* First Release of the Witlee for WP plugin

== Upgrade Notice ==

= 1.0 =
* Just released!!!!